package br.up.edu.oapp09;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void calcular(View v){

        EditText cxTempo = (EditText) findViewById(R.id.txtTempo);
        EditText cxVelocidade = (EditText) findViewById(R.id.txtVelocidade);
        EditText cxConsumo = (EditText) findViewById(R.id.txtConsumo);

        String txtTempo = cxTempo.getText().toString();
        String txtVelocidade = cxVelocidade.getText().toString();

        double tempo = Double.parseDouble(txtTempo);
        double velocidade = Double.parseDouble(txtVelocidade);

        double distancia = velocidade * tempo;
        double consumo = distancia / 12;

        cxConsumo.setText(String.valueOf(consumo));
    }
}
